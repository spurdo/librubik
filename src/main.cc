#include <fstream>
#include <iostream>

#include "librubik/cube.hh"
#include "librubik/matrix3D.hh"

using namespace rubik;

int main()
{
    Move m;
    Cube cube;

    m.mvt = FRONT;
    m.dir = CLOCKWISE;

    cube.do_moves(
        parse_moves("F' D' R2 x L' U B R F U2 F2 L2 L y2 F' B2 U2 D B' z'"));

    std::cout << cube;

    return 0;
}
