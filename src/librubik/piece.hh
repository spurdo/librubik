#pragma once

#include "color.hh"
#include "move.hh"
#include "vector3D.hh"

namespace rubik
{
    class Piece
    {
    public:
        Piece(Vector3D<int> coords, Vector3D<Color> colors);

        enum Type
        {
            CENTER = 0,
            EDGE,
            CORNER,
        };

        Type get_type();

        bool operator==(const Piece &other);
        bool operator<(const Piece &other);
        bool operator!=(const Piece &other);
        bool operator>(const Piece &other);
        bool operator<=(const Piece &other);
        bool operator>=(const Piece &other);

        Vector3D<int> coords;
        Vector3D<Color> colors;

        void do_move(Face face, Direction dir);
        void do_move(Axis axis, Direction dir);
    };

    std::ostream &operator<<(std::ostream &os, const Piece &p);
} // namespace rubik
