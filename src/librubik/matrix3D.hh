#pragma once

#include <array>

#include "vector3D.hh"

namespace rubik
{
    template <typename T>
    class Matrix3D
    {
    public:
        Matrix3D(std::array<std::array<T, 3>, 3> matrix)
            : matrix_(matrix)
        {}

        T at(int i, int j)
        {
            return matrix_[i][j];
        }

        Vector3D<T> operator*(const Vector3D<T> &v)
        {
            int x =
                matrix_[0][0] * v.x + matrix_[0][1] * v.y + matrix_[0][2] * v.z;

            int y =
                matrix_[1][0] * v.x + matrix_[1][1] * v.y + matrix_[1][2] * v.z;

            int z =
                matrix_[2][0] * v.x + matrix_[2][1] * v.y + matrix_[2][2] * v.z;

            return Vector3D<T>(x, y, z);
        }

        void transpose()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    if (i != j)
                        std::swap(matrix_[j][i], matrix_[i][j]);
                }
            }
        }

    private:
        std::array<std::array<T, 3>, 3> matrix_;
    };
} // namespace rubik
