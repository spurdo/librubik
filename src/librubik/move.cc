#include "move.hh"

#include <iostream>
#include <stdexcept>

namespace rubik
{
    static size_t split(const std::string &src, std::vector<std::string> &vec,
                        char c)
    {
        size_t pos = src.find(c);
        size_t initialPos = 0;

        while (pos != std::string::npos)
        {
            vec.push_back(src.substr(initialPos, pos - initialPos));
            initialPos = pos + 1;

            pos = src.find(c, initialPos);
        }

        vec.push_back(
            src.substr(initialPos, std::min(pos, src.size()) - initialPos + 1));

        return vec.size();
    }

    static std::variant<Face, Axis> get_mvt(char c)
    {
        if (c == 'F')
            return FRONT;
        else if (c == 'B')
            return BACK;
        else if (c == 'U')
            return UP;
        else if (c == 'D')
            return DOWN;
        else if (c == 'L')
            return LEFT;
        else if (c == 'R')
            return RIGHT;
        else if (c == 'x')
            return X;
        else if (c == 'y')
            return Y;
        else if (c == 'z')
            return Z;

        throw std::invalid_argument("Invalid command\n");
    }

    static Move treat_token(std::string &token)
    {
        Move move;
        size_t len = token.length();

        move.mvt = get_mvt(token[0]);

        if (len == 2)
        {
            if (token[1] == '\'')
                move.dir = ANTI_CLOCKWISE;
            else if (token[1] == '2')
                move.is_double = true;
        }

        return move;
    }

    std::vector<Move> parse_moves(std::string input)
    {
        std::vector<Move> res;
        std::vector<std::string> vec;
        vec.clear();
        res.clear();

        split(input, vec, ' ');

        for (auto &token : vec)
            res.push_back(treat_token(token));

        return res;
    }
} // namespace rubik
