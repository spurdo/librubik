#pragma once

#include <string>
#include <variant>
#include <vector>

namespace rubik
{
    enum Axis
    {
        X = 0,
        Y,
        Z,
    };

    enum Direction
    {
        CLOCKWISE = 0,
        ANTI_CLOCKWISE,
    };

    enum Face
    {
        UP = 0,
        DOWN,
        LEFT,
        RIGHT,
        FRONT,
        BACK,
    };

    class Move
    {
    public:
        std::variant<Face, Axis> mvt;
        Direction dir = Direction::CLOCKWISE;
        bool is_double = false;
    };

    std::vector<Move> parse_moves(std::string input);

} // namespace rubik
