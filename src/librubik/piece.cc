#include "piece.hh"

#include <array>

#include "matrix3D.hh"

namespace rubik
{
    Piece::Piece(Vector3D<int> coords, Vector3D<Color> colors)
        : coords(coords)
        , colors(colors)
    {}

    std::ostream &operator<<(std::ostream &os, const Piece &p)
    {
        os << p.coords << ' ' << p.colors;
        return os;
    }

    Piece::Type Piece::get_type()
    {
        if ((coords.x == 0 && coords.y != 0 && coords.z != 0)
            || (coords.x != 0 && coords.y == 0 && coords.z != 0)
            || (coords.x != 0 && coords.y != 0 && coords.z == 0))

            return Type::EDGE;

        if ((coords.x != 0 && coords.y == 0 && coords.z == 0)
            || (coords.x == 0 && coords.y != 0 && coords.z == 0)
            || (coords.x == 0 && coords.y == 0 && coords.z != 0))

            return Type::CENTER;

        return Type::CORNER;
    }

    bool Piece::operator==(const Piece &other)
    {
        return coords == other.coords && colors == other.colors;
    }

    bool Piece::operator<(const Piece &other)
    {
        return (coords.x < other.coords.x && coords.y < other.coords.y
                && coords.z < other.coords.z);
    }

    bool Piece::operator!=(const Piece &other)
    {
        return coords != other.coords || colors != other.colors;
    }

    bool Piece::operator>(const Piece &other)
    {
        return (coords.x > other.coords.x && coords.y > other.coords.y
                && coords.z > other.coords.z);
    }

    bool Piece::operator<=(const Piece &other)
    {
        return (*this == other || *this < other);
    }

    bool Piece::operator>=(const Piece &other)
    {
        return (*this == other || *this > other);
    }

    void Piece::do_move(Face face, Direction dir)
    {
        std::array<std::array<int, 3>, 3> r;

        if (face == FRONT || face == BACK)
        {
            r = { { { 1, 0, 0 }, { 0, 0, -1 }, { 0, 1, 0 } } };
            std::swap(colors.y, colors.z);
        }
        else if (face == RIGHT || face == LEFT)
        {
            r = { { { 0, 0, 1 }, { 0, 1, 0 }, { -1, 0, 0 } } };
            std::swap(colors.x, colors.z);
        }
        else if (face == UP || face == DOWN)
        {
            r = { { { 0, -1, 0 }, { 1, 0, 0 }, { 0, 0, 1 } } };
            std::swap(colors.x, colors.y);
        }

        Matrix3D<int> rot(r);

        if (face == FRONT || face == RIGHT || face == UP)
            rot.transpose();

        if (dir == ANTI_CLOCKWISE)
            rot.transpose();

        coords = rot * coords;
    }

    void Piece::do_move(Axis axis, Direction dir)
    {
        if (axis == X)
            do_move(RIGHT, dir);

        else if (axis == Y)
            do_move(UP, dir);

        else if (axis == Z)
            do_move(FRONT, dir);
    }
} // namespace rubik
