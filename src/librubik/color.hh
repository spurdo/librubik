#pragma once

#include <ostream>

namespace rubik
{
    enum Color
    {
        WHITE = 0,
        YELLOW,
        GREEN,
        BLUE,
        RED,
        ORANGE,
        NONE
    };

    inline std::ostream &operator<<(std::ostream &os, const Color &c)
    {
        switch (c)
        {
        case 0:
            os << 'W';
            break;
        case 1:
            os << 'Y';
            break;
        case 2:
            os << 'G';
            break;
        case 3:
            os << 'B';
            break;
        case 4:
            os << 'R';
            break;
        case 5:
            os << 'O';
            break;
        default:
            os << 'X';
            break;
        }

        return os;
    }

} // namespace rubik
