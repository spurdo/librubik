#include "parse_csv.hh"

#include <fstream>
#include <stdexcept>

namespace rubik::solver
{
    static size_t split(const std::string &src, std::vector<std::string> &vec,
                        char c)
    {
        size_t pos = src.find(c);
        size_t initialPos = 0;

        while (pos != std::string::npos)
        {
            vec.push_back(src.substr(initialPos, pos - initialPos));
            initialPos = pos + 1;

            pos = src.find(c, initialPos);
        }

        vec.push_back(
            src.substr(initialPos, std::min(pos, src.size()) - initialPos + 1));

        return vec.size();
    }

    std::vector<std::vector<std::string>> parse_csv(const std::string &f)
    {
        std::ifstream input;

        bool first = true;

        size_t std_size;
        size_t cur_size;
        size_t line_num = 0;

        input.open(f);

        if (!input.is_open())
            throw std::ios_base::failure("Error opening file");

        std::vector<std::vector<std::string>> res;
        std::string token;

        while (std::getline(input, token))
        {
            line_num++;
            std::vector<std::string> token_vec;

            if (first)
            {
                std_size = split(token, token_vec, ',');
                cur_size = std_size;
                first = false;
            }

            else
                cur_size = split(token, token_vec, ',');

            if (cur_size != std_size)
            {
                std::string msg = "Non consistent number of columns at line ";
                msg += std::to_string(line_num);
                throw std::ios_base::failure(msg);
            }

            res.push_back(token_vec);
        }

        return res;
    }
} // namespace rubik::solver
