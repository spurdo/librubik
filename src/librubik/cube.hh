#pragma once

#include <istream>
#include <vector>

#include "piece.hh"

namespace rubik
{
    class Cube
    {
    public:
        Cube();

        static Cube from_stream(std::istream &is);

        using iterator = std::vector<Piece>::iterator;
        using const_iterator = std::vector<Piece>::const_iterator;

        iterator begin();
        iterator end();
        const_iterator begin() const;
        const_iterator end() const;

        Piece find_piece(const Vector3D<int> coords) const;
        Piece find_piece(const Vector3D<Color> colors) const;

        void do_move(Move move);
        void do_moves(std::vector<Move> moves);
        void undo_move(Move move);
        void undo_moves(std::vector<Move> moves);

        friend std::ostream &operator<<(std::ostream &os, Cube cube);

    private:
        std::vector<Piece> pieces_;
    };

    inline std::ostream &operator<<(std::ostream &os, Cube cube)
    {
        for (auto piece : cube.pieces_)
            os << piece << '\n';
        return os;
    }

} // namespace rubik
