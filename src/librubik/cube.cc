#include "cube.hh"

#include <algorithm>
#include <iostream>
#include <variant>

namespace rubik
{
    /* Extra functions */

    static Piece create_piece(int x, int y, int z)
    {
        Color cx = NONE;
        Color cy = NONE;
        Color cz = NONE;

        switch (x)
        {
        case 1:
            cx = GREEN;
            break;
        case -1:
            cx = BLUE;
            break;
        }

        switch (y)
        {
        case 1:
            cy = RED;
            break;
        case -1:
            cy = ORANGE;
            break;
        }

        switch (z)
        {
        case 1:
            cz = WHITE;
            break;
        case -1:
            cz = YELLOW;
            break;
        }

        return Piece({ x, y, z }, { cx, cy, cz });
    }

    static Color char_clr(char c)
    {
        switch (c)
        {
        case 'R':
            return RED;
        case 'G':
            return GREEN;
        case 'B':
            return BLUE;
        case 'W':
            return WHITE;
        case 'Y':
            return YELLOW;
        case 'O':
            return ORANGE;
        case 'X':
            return NONE;
        }

        throw std::invalid_argument("Invalid color!\n\
                                    Please select either R G B W Y O or X");
    }

    static bool should_move(Piece piece, Face face)
    {
        if ((face == UP && piece.coords.z == 1)
            || (face == DOWN && piece.coords.z == -1)
            || (face == RIGHT && piece.coords.y == 1)
            || (face == LEFT && piece.coords.y == -1)
            || (face == FRONT && piece.coords.x == 1)
            || (face == BACK && piece.coords.x == -1))
            return true;

        return false;
    }

    Cube::Cube()
    {
        pieces_.reserve(27);

        for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++)
                for (int z = 0; z < 3; z++)
                    pieces_.emplace_back(create_piece(x - 1, y - 1, z - 1));
    }

    Cube Cube::from_stream(std::istream &is)
    {
        Cube c = Cube();

        int x, y, z;
        char cx, cy, cz, tok;

        auto begin = c.begin();

        while (is >> tok >> x >> tok >> y >> tok >> z >> tok)
        {
            is >> tok >> cx >> tok >> cy >> tok >> cz >> tok;

            Vector3D<int> coords = { x, y, z };
            Vector3D<Color> colors = { char_clr(cx), char_clr(cy),
                                       char_clr(cz) };

            (*begin++) = Piece(coords, colors);
        }

        return c;
    }

    Cube::iterator Cube::begin()
    {
        return pieces_.begin();
    }

    Cube::iterator Cube::end()
    {
        return pieces_.end();
    }

    Cube::const_iterator Cube::begin() const
    {
        return pieces_.begin();
    }

    Cube::const_iterator Cube::end() const
    {
        return pieces_.end();
    }

    Piece Cube::find_piece(Vector3D<int> coords) const
    {
        for (auto piece : pieces_)
        {
            if (piece.coords == coords)
                return piece;
        }

        throw std::invalid_argument("Invalid piece coordinates!");
    }

    Piece Cube::find_piece(Vector3D<Color> colors) const
    {
        std::vector<Color> vec = { colors.x, colors.y, colors.z };

        for (auto piece : pieces_)
        {
            std::vector<Color> color_vec = { piece.colors.x, piece.colors.y,
                                             piece.colors.z };

            if (std::is_permutation(vec.begin(), vec.end(), color_vec.begin(),
                                    color_vec.end()))
                return piece;
        }

        throw std::invalid_argument("Invalid piece colors!");
    }

    void Cube::do_move(Move move)
    {
        if (std::holds_alternative<Axis>(move.mvt))
        {
            Axis movement = std::get<Axis>(move.mvt);
            for (auto &piece : pieces_)
                piece.Piece::do_move(movement, move.dir);
        }

        else
        {
            Face movement = std::get<Face>(move.mvt);
            for (auto &piece : pieces_)
                if (should_move(piece, movement))
                    piece.Piece::do_move(movement, move.dir);
        }

        if (move.is_double)
        {
            move.is_double = false;
            Cube::do_move(move);
        }
    }

    void Cube::do_moves(std::vector<Move> moves)
    {
        for (Move move : moves)
            Cube::do_move(move);
    }

    void Cube::undo_move(Move move)
    {
        Direction undo = move.dir == CLOCKWISE ? ANTI_CLOCKWISE : CLOCKWISE;
        move.dir = undo;
        do_move(move);
    }

    void Cube::undo_moves(std::vector<Move> moves)
    {
        for (Move move : moves)
            Cube::undo_move(move);
    }
} // namespace rubik
