CXX = g++
CXXFLAGS = -Wall -Wextra -Werror -pedantic -std=c++17
CPPFLAGS = -Isrc/

OBJ = src/librubik/cube.o               \
      src/librubik/move.o               \
      src/librubik/piece.o              \
      src/librubik/solver/f2l.o         \
      src/librubik/solver/parse_csv.o

CLI_OBJ = src/main.o $(OBJ)
CLI_BIN = cli

TESTS_OBJ = tests/syntax_core.o tests/syntax_additional.o $(OBJ)
TESTS_BIN = unit_tests

all: $(CLI_BIN)

$(CLI_BIN): $(CLI_OBJ)
	$(LINK.cc) $^ $(LDLIBS) -o $@

$(TESTS_BIN): LDLIBS += -lcriterion
$(TESTS_BIN): $(TESTS_OBJ)
	$(LINK.cc) $^ $(LDLIBS) -o $@

check: $(TESTS_BIN)
	./$(TESTS_BIN)

clean:
	$(RM) $(OBJ) $(CLI_OBJ) $(CLI_BIN) $(TESTS_BIN) $(TESTS_OBJ)

.PHONY: all check clean
